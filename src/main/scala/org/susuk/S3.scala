package org.susuk

import java.io.File

import cats.ApplicativeError
import cats.data.{EitherT, ReaderT}
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model._
import monix.cats._
import monix.eval.Task

import eu.timepit.refined._
import eu.timepit.refined.auto._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.{MinSize, MaxSize}

import scala.language.{higherKinds, implicitConversions}

trait S3[F[?]] {
  type ErrorOr[A] = EitherT[F, Throwable, A]

  type BucketNamePredicate = And[MinSize[W.`3`.T], MaxSize[W.`37`.T]]
  type BucketName = String Refined BucketNamePredicate

  implicit class StringOps(str: String) {
    def bucketName: BucketName =
      Refined.unsafeApply[String, BucketNamePredicate](str)
  }

  def createBucketT(bucketName: BucketName)(
      implicit AE: ApplicativeError[F, Throwable]): ReaderT[ErrorOr, AmazonS3Client, Bucket] =
    ReaderT[ErrorOr, AmazonS3Client, Bucket] { s3Client =>
      AE.attemptT {
        AE.pure {
          val bucket = s3Client.createBucket(new CreateBucketRequest(bucketName))
          s3Client.setBucketVersioningConfiguration(
            new SetBucketVersioningConfigurationRequest(
              bucketName,
              new BucketVersioningConfiguration(BucketVersioningConfiguration.ENABLED)))
          bucket
        }
      }
    }

  def uploadObject(bucketName: String)(file: File)(
      implicit AE: ApplicativeError[F, Throwable]): ReaderT[ErrorOr, AmazonS3Client, PutObjectResult] =
    ReaderT[ErrorOr, AmazonS3Client, PutObjectResult] { s3Client =>
      AE.attemptT {
        AE.pure {
          s3Client.putObject(new PutObjectRequest(bucketName, file.getName, file))
        }
      }
    }

}

object S3Task extends S3[Task]

object Main extends App {
  import S3Task._

  val file = new File("")

  val x = "1"

  val t = for {
    bucket <- S3Task.createBucketT("1".bucketName)
    putResult <- S3Task.uploadObject(bucket.getName)(file)
  } yield {
    putResult
  }

  t.run(null)

}
